/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   alist_new.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/14 14:18:03 by bperreon          #+#    #+#             */
/*   Updated: 2015/12/14 14:28:32 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "array_list.h"

t_alist			*alist_new(size_t const content_size)
{
	t_alist		*new;

	if (!(new = (t_alist*)malloc(sizeof(t_alist))))
		return (NULL);
	if (!(new->data = calloc(DEFAULT_CAPACITY, content_size)))
		return (NULL);
	new->size = 0;
	new->content_size = content_size;
	new->allocated = DEFAULT_CAPACITY;
	return (new);
}
