/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   alist_delete.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/14 14:28:54 by bperreon          #+#    #+#             */
/*   Updated: 2015/12/14 14:31:23 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "array_list.h"

void		alist_delete(t_alist **list)
{
	if (!list || !*list)
		return ;
	bzero((*list)->data, (*list)->content_size * (*list)->allocated);
	free((*list)->data);
	(*list)->data = NULL;
	bzero(*list, sizeof(t_alist));
	free(*list);
	*list = NULL;
}
