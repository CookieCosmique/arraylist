/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   alist_remove.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/14 14:34:12 by bperreon          #+#    #+#             */
/*   Updated: 2015/12/14 14:37:37 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "array_list.h"

t_alist		*alist_remove(t_alist *list, size_t const index)
{
	if (!list)
		return (NULL);
	if (index >= list->size)
		return (list);
	memmove(list->data + list->content_size * index,
			list->data + list->content_size * (index + 1), list->size - index);
	list->size--;
	return (list);
}
