/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   alist_add.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/14 14:20:28 by bperreon          #+#    #+#             */
/*   Updated: 2015/12/14 14:57:08 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "array_list.h"

t_alist		*alist_add(t_alist *list, void const *data)
{
	if (!list)
		return (NULL);
	if (list->size == list->allocated)
	{
		if (!(list->data = realloc(list->data, list->content_size *
						list->allocated * 2)))
			return (NULL);
		list->allocated <<= 1;
	}
	memcpy(list->data + list->content_size * list->size, data,
			list->content_size);
	list->size++;
	return (list);
}
