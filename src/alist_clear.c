/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   alist_clear.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/14 14:25:26 by bperreon          #+#    #+#             */
/*   Updated: 2015/12/14 14:28:08 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "array_list.h"

t_alist		*alist_clear(t_alist *list)
{
	if (!list)
		return (NULL);
	bzero(list->data, list->content_size * list->size);
	list->size = 0;
	if (list->allocated != DEFAULT_CAPACITY)
	{
		list->allocated = DEFAULT_CAPACITY;
		if (!(list->data = realloc(list->data, list->content_size *
						DEFAULT_CAPACITY)))
			return (NULL);
	}
	return (list);
}
