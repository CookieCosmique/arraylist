/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   array_list.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/25 15:50:42 by bperreon          #+#    #+#             */
/*   Updated: 2015/12/14 14:28:46 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ARRAY_LIST_H
# define ARRAY_LIST_H

# include <stdlib.h>
# include <string.h>
# include <stdio.h>

# define DEFAULT_CAPACITY 128

typedef struct		s_alist
{
	void	*data;
	size_t	content_size;
	size_t	size;
	size_t	allocated;
}					t_alist;

t_alist				*alist_new(size_t const content_size);
t_alist				*alist_add(t_alist *list, void const *data);
t_alist				*alist_remove(t_alist *list, size_t const index);
t_alist				*alist_clear(t_alist *list);
void				alist_delete(t_alist **list);

#endif
